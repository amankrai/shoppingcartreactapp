import React from 'react';
import Login from './login/login';
import Signup from './signup/signup';
import Home from './home/home';
import Header from './header/header';
import Cart from './cart/cart';
import AddProduct from './addProduct/addProduct';
import Product from './product/product';
import CategoryView from './views/categoryView';
import SubCategoryView from './views/subCategoryView';
import Checkout from './checkout/checkout';
import OrderConfirmation from './checkout/orderConfirmation';
import Order from './order/order';
import OrderReview from './order/orderReview';
import Profile from './profile/profile';
import { Route, Switch } from 'react-router'
import { BrowserRouter as Router } from 'react-router-dom';

function route() {
    return (
        <Router>
            <>
                <Header />
                <Switch>
                    <Route path='/' component={Home} exact />
                    <Route path='/login' component={Login} exact />
                    <Route path='/signup' component={Signup} exact />
                    <Route path='/product/:id' component={Product} exact />
                    <Route path="/category/:id" render={(props) => <CategoryView {...props} />} />
                    {/* <Route path='/category/:id' component={CategoryView} exact /> */}
                    <Route path='/subcategory/:id' component={SubCategoryView} exact />
                    <Route path='/cart' component={() => <Cart />} exact />
                    <Route path='/add-product' component={() => <AddProduct />} exact />
                    <Route path='/checkout' component={() => <Checkout />} exact />
                    <Route path='/order' component={() => <Order />} exact />
                    <Route path='/profile' component={() => <Profile />} exact />
                    <Route path='/OrderReview/:id' component={OrderReview} exact />
                    <Route path='/orderConfirmation/' component={OrderConfirmation} exact />
                    <Route path='/*' component={Home} exact />
                </Switch>
            </>
        </Router>
    )
}
export default route;