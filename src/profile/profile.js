import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUser, getAddress, editAddress, updateUser } from './profileServices';
import '../profile/profile.css';
class Profile extends Component {
    constructor(props) {
        super(props)
        let userId = this.props.user.length > 0 ? JSON.parse(this.props.user)['id'] : null;
        this.state = {
            userId: userId,
            user: {},
            address: [],
            editAddress: false,
            AddressObject: {},
            isHovering: false,
            showUserEditForm: false
        };
    }
    updateComponentWithAPICall = () => {
        getUser(this.state.userId).then((response) => {
            if (response.status === 200) {
                this.setState({
                    user: response.data.user
                })
            }
        })
        getAddress(this.state.userId).then((response) => {
            if (response.status === 200) {
                let result = response.data.results;
                if (result) {
                    this.setState({
                        address: result
                    });
                }
				else
				{
					console.log(response.data.message)
				}
            }
        });
    }
    componentWillMount() {
        this.updateComponentWithAPICall();
    }

    handleChange = (event, formName) => {
        let name = event.target.name;
        let value = event.target.value;
        if (formName === 'userForm') {
            this.setState({
                user: {
                    ...this.state.user,
                    [name]: value
                }
            });
        }
        else if (formName === 'addressForm') {
            this.setState({
                AddressObject: {
                    ...this.state.AddressObject,
                    [name]: value,
                }
            });
        }
    }

    handleSubmit = (event, formName) => {
        event.preventDefault();
        if (formName === 'userForm') {
            let userId = this.state.userId;
            let userObject = {
                Name: this.state.user.Name,
            }
            let sessionToken = this.props.user.length > 0 ? JSON.parse(this.props.user)['sessionToken'] : null;
            updateUser(userId, userObject, sessionToken)
                .then((response) => {
                    if (response.status === 200) {
                        this.updateComponentWithAPICall();
                        this.setState({
                            showUserEditForm: false
                        });

                    }
                }).catch(function (error) {
                    if (error.response) {
                        console.log(error.response.data.error);
                    }
                });
        }
        else if (formName === 'addressForm') {
            editAddress(this.state.AddressObject).then((response) => {
                if (response.status === 200) {
                    this.updateComponentWithAPICall();
                    this.setState({
                        editAddress: false
                    });
                }
            }).catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });
        }
    }

    handleMouseHover = () => {
        this.setState(this.toggleHoverState);
    }

    toggleHoverState = (state) => {
        return {
            isHovering: !state.isHovering,
        };
    }

    editAddress = (id) => {
        if (id !== null) {
            let currentEditingObject;
            let address = this.state.address;
            for (let i = 0; i < address.length; i++) {
                if (address[i].id === id) {
                    currentEditingObject = address[i]
                    break;
                }
            }
            this.setState({
                ...this.state,
                editAddress: id,
                AddressObject: currentEditingObject
            });
        }
        else {
            this.setState({
                editAddress: false,
                AddressObject: {}
            })
        }
    }

    editName = () => {
        this.setState({
            showUserEditForm: true,
        })
    }
    renderUser = () => {
        let user = this.state.user;
        let userUi;
        user.id ?
            userUi = <>
                <div className="container">
                    <div className="user-profile-photo"><img src={user.profilePhoto} alt="profile pic" /></div>
                    <div className="user">
                        {
                            this.state.showUserEditForm ?
                                <form onSubmit={(e) => this.handleSubmit(e, 'userForm')}>
                                    <div className="form-group d-inline-block mr-2">
                                        <input type="text" className="form-control" id="name" name="name" onChange={(e) => this.handleChange(e, 'userForm')} value={this.state.user.name} />
                                    </div>
                                    <button type="submit" className="btn btn-info mr-2">Update</button>
                                    <button type="cancel" className="btn btn-danger">X</button>
                                </form>
                                :
                                <div
                                    className="name"
                                    onMouseEnter={this.handleMouseHover}
                                    onMouseLeave={this.handleMouseHover}
                                >
                                    {user.name}
                                    {this.state.isHovering &&
                                        <i className="fa fa-pencil ml-5" aria-hidden="true" onClick={this.editName}></i>}
                                </div>
                        }
                        <div className="username">@{user.userName}</div>
                    </div>
                </div>
            </>
            :
            userUi = '';
        return userUi;
    }
    renderAddress = () => {
        let addresss = this.state.address;
        const addressUI = addresss.map((address, key) => {
            return (
                this.state.editAddress === address.id ?
                    <div className="col-md-4" key={key} id={address.id}>
                        <form onSubmit={(e) => this.handleSubmit(e, 'addressForm')}>
                            <div className="card h-100">
                                <div className="card-header px-2">Address Type : {address.AddressType}
                                    <button className="btn btn-danger float-right ml-2" type="submit">Save</button>
                                    <button className="btn btn-dark float-right" type="cancel">Cancel</button>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="addressLine1">Address Line 1</label>
                                        <input type="text" className="form-control" id="addressLine1" name="addressLine1" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.addressLine1} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="addressLine2">Address Line 2</label>
                                        <input type="text" className="form-control" id="addressLine2" name="addressLine2" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.addressLine2} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="street">Street </label>
                                        <input type="text" className="form-control" id="street" name="street" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.street} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="locality">Locality </label>
                                        <input type="text" className="form-control" id="locality" name="locality" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.locality} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="city">City </label>
                                        <input type="text" className="form-control" id="city" name="city" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.city} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="state">State </label>
                                        <input type="text" className="form-control" id="state" name="state" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.state} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="postalCode">Postal Code </label>
                                        <input type="number" className="form-control" id="postalCode" name="postalCode" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.postalCode} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="country">Country </label>
                                        <input type="text" className="form-control" id="country" name="country" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.country} />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    :
                    <div className="col-md-4" key={key} id={address.id}>
                        <div className="card h-100">
                            <div className="card-header"><i className="fa fa-address-book" aria-hidden="true"></i>&nbsp;{address.addressType}
                                <button className="btn btn-info float-right" onClick={() => this.editAddress(address.id)}>Edit</button>
                            </div>
                            <div className="card-body">
                                <div>Address Line 1 : {address.addressLine1}</div>
                                {address.AddressLine2 ? <div>Address Line 2 : {address.addressLine2}</div> : ''}
                                <div>Street : {address.street}</div>
                                <div>Locality : {address.locality}</div>
                                <div>City : {address.city}</div>
                                <div>{address.state}-{address.postalCode}</div>
                                <div>{address.country}</div>
                            </div>
                        </div>
                    </div>
            )
        });
        return addressUI;
    }


    render() {
        return <div className="profile">
            <div className="overlay">
                <img src={require('../images/texture.jpg')} alt="overlay" />
            </div>
            <div className="user-detail">{this.renderUser()}</div>
            <div className="address container">
                <div className="row">{this.renderAddress()}</div>
            </div>
        </div>
    }
}
function mapStateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn })
}
export default connect(
    mapStateToProps)(
        Profile
    );