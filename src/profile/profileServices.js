import axios from 'axios';
import {
    HEADERS,
    BASEURL
} from '../constant/constant';
export const getUser = (userId) => {
    let url = `${BASEURL}user/${userId}`;
    return axios.get(url);
}
export const getAddress = (userId) => {
    let url = `${BASEURL}address/${userId}`;
    return axios.get(url);
}
export const editAddress = (addressObject) => {
    let url = `${BASEURL}editAddress`;
    delete addressObject.user_id;
    delete addressObject.createdAt;
    delete addressObject.updatedAt;
    console.log('here', addressObject)
    return axios.put(url, addressObject);
}

export const updateUser = (userId, userObject, sessionToken) => {
    let headers = HEADERS;
    headers['Content-Type'] = 'application/json';
    headers['X-Parse-Session-Token'] = sessionToken;
    let url = `${BASEURL} users / ${userId} `;
    return axios.put(url, userObject, { headers });
}