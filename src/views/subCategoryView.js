import React, {
    Component
} from 'react';
import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';
import { Link } from "react-router-dom";
import { isNull } from 'util';


class SubCategoryView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
        }
    }
    getProduct = () => {
        let url = `${BASEURL}product/get-product-by-filter?subCategory_id=${this.props.match.params.id}`;
        axios.get(url).then((response) => {
            if (response.data) {
                this.setState({
                    products: response.data.results,
                    productOfId: this.props.match.params.id
                })
            }
        });
    }
    componentDidMount() {
        this.getProduct();
    }
    componentDidUpdate() {
        if (this.state.productOfId !== this.props.match.params.id) {
            this.getProduct();
        }
    }
    static getDerivedStateFromProps(props, state) {
        return { id: props.match.params.id };
    }

    renderProduct = () => {
        let productUi = this.state.products.map((product, key) => {
            let imageValue = product.images[0] ? product.images[0].data : null;
            let productName = product.productName.substring(0, 23) + "...";
            let url = "/product/" + product.id;
            return <Link to={url} key={key} className="w-25">
                <div className="card mt-2 mr-1">
                    <div className="card-image">
                        <img className="card-img-top" src={imageValue} alt="Card cap" />
                    </div>
                    <div className="card-body">
                        <p className="card-title">{productName}</p>
                        <p className="card-text"><b>$ {product.price}</b></p>
                    </div>
                </div>
            </Link>
        });
        if (productUi.every(isNull)) {
            productUi = <div className="jumbotron w-100 text-center">
                <img src={require('../images/noProduct.png')} alt="No Product to display" />
                <h3>Sorry, no results found!
                Please check the spelling or try searching for something else.
                </h3>
            </div>
        }
        return productUi;
    }

    render() {
        return (
            <div className="subCategory-view d-flex mx-5 flex-wrap">
                {this.renderProduct()}
            </div>
        );
    }
}
export default SubCategoryView;