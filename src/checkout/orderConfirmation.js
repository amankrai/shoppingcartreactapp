import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { placeOrder, deleteCart } from './orderConfirmationServices';
import '../checkout/orderConfirmation.css';


class OrderConfirmation extends Component {
    constructor(porps) {
        super(porps);
        this.state = {
            processing: true,
            paymentStatus: false,
            errorMessage: ''
        };
    }
    queryStringParse = (string) => {
        let parsed = {}
        if (string !== '') {
            string = string.substring(string.indexOf('?') + 1)
            let p1 = string.split('&')
            p1.map(function (value) {
                let params = value.split('=')
                parsed[params[0]] = params[1]
                return null;
            });
        }
        return parsed;
    }
    async componentDidMount() {
        const queryParams = await this.queryStringParse(this.props.location.search);
        if (queryParams.payment_id !== "null") {
            this.setState({
                payment_id: queryParams.payment_id,
                paymentStatus: true,
            })
        }
        const object = {
            payment_id: queryParams.payment_id,
            user_id: queryParams.user_id,
            payment_status: queryParams.payment_status
        }
        placeOrder(object).then((response) => {
            if (response.status === 200) {
                let result = response.data.results;
                if (result) {
                    deleteCart(queryParams.user_id);
                    setTimeout(() => {
                        this.setState({
                            orderId: result.id,
                            processing: false
                        });
                    }, 1500);
                }
                else if (response.data.error) {
                    this.setState({
                        errorMessage: response.data.error,
                    });
                }

            }
        }).catch((error) => {
            if (error.response) {
                console.log(error.response.data.error);
            }
        });

    }
    render() {
        console.log(this.state);
        const url = `/orderReview/${this.state.orderId}`;
        return <div className="order-confirmation">
            {this.state.errorMessage ?
                <>
                    <div className="alert alert-warning alert-dismissible fade show text-center sticky-top mb-1" role="alert">
                        <strong>{this.state.errorMessage}</strong>
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="jumbotron text-center">
                        <img src={require('../images/error.jpg')} alt="ERROR" height="200px" width="30%" />
                        <h4 className="mt-4">Sorry ! your payment failed due to some reason. If the amount is deducted from your account, it will be refunded.</h4>

                    </div>
                </>
                :
                (
                    this.state.processing ?
                        <div className="text-center">
                            <img src={require('../images/processing.gif')} alt="payment is processing" />
                        </div>
                        :
                        <div className="jumbotron text-center">
                            <i class="fa fa-check-circle-o" height="200px" width="30%" aria-hidden="true"></i>
                            <div>
                                <h3>
                                    Thank You for placing your order.<br />
                                    Your order Id is : <Link to={url}><h3 className="d-inline-block">{this.state.orderId}</h3></Link>
                                </h3>
                            </div>
                        </div>
                )
            }
        </div>
    }
}
export default OrderConfirmation;