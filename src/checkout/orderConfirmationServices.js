import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';

export const placeOrder = (object) => {
    let url = `${BASEURL}place-order`;
    return axios.post(url, object);
}
export const deleteCart = (userId) => {
    let url = `${BASEURL}cart-remove/${userId}`;
    return axios(url);
}