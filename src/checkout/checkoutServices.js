import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';

export const placeOrder = (object) => {
    let url = `${BASEURL}payment/request`;
    return axios.post(url, object);
}

export const getCartItems = (userId) => {
    let obj = {
        user_id: userId,
    }
    let url = `${BASEURL}cart`;
    return axios.post(url, obj);
}

export const getAdrress = (userId) => {
    let url = `${BASEURL}address/${userId}`;
    // let url = `http://localhost:1337/address/2`;
    return axios.get(url);
}
