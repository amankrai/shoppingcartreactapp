import React, {
    Component
} from 'react';
import '../checkout/checkout.css';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { placeOrder, getCartItems, getAdrress } from './checkoutServices';

class Checkout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orderPlaced: false,
            AddressObject: {
                addressLine1: '',
                addressLine2: '',
                street: '',
                locality: '',
                city: '',
                state: '',
                postalCode: '',
                country: '',
                addressType: ''
            },
            Address: [],
            error: {},
            cart: [],
            tagName: 'span'
        };
    }

    componentDidMount = () => {
        let user = window.localStorage.getItem('user');
        let userId = user ? JSON.parse(user).id : null;
        if (userId === null) {
            return;
        }
        getCartItems(userId)
            .then((response) => {
                let data = response.data.results;
                if (data) {
                    this.setState({
                        cart: data,
                    });
                }
            })
            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });

        getAdrress(userId).then((response) => {
            if (response.data) {
                this.setState({
                    Address: response.data.results
                })
            }
        })
            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });
    }

    handleChange = (event, formName) => {
        let name = event.target.name;
        let value = event.target.value;
        let error = this.state.error;
        error[`${name}Error`] = null;
        if (formName === 'addressForm') {
            this.setState({
                AddressObject: {
                    ...this.state.AddressObject,
                    [name]: value,
                },
                error: error
            });
        }
    }

    validate = () => {
        const addressObject = this.state.AddressObject;
        let flag = false;
        let error = this.state.error;
        for (let field in addressObject) {
            if (addressObject[field] === '' && field !== 'addressLine2') {
                error[`${field}Error`] = "*Required Feild";
                flag = true;
                console.log(field, flag);

            }
            else {
                error[`${field}Error`] = null;
            }
        }
        this.setState({
            error: error
        });
        return flag;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.validate()) {
            return;
        }
        const totalPrice = this.calcTotalPrice(this.state.cart);
        const user = JSON.parse(this.props.user);
        const object = {
            "purpose": "Pruchase from shoppingCart",
            "amount": totalPrice,
            "buyer_name": user.name,
            "email": user.emailAddress,
            "phone": "1234567890",
            "send_email": false,
            "send_sms": false,
            "user_id": user.id
        };
        placeOrder(object).then((response) => {
            let data = response.data;
            if (data) {
                window.location.href = data.url;
            }
        })
            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });
    }

    calcTotalItems = (cart) => {
        let totalItems = 0;
        cart.map((item) => {
            totalItems = totalItems + parseInt(item.quantity);
            return null;
        });
        return totalItems;
    }

    calcTotalPrice = (cart) => {
        let totalPrice = 0;
        cart.map((item) => {
            totalPrice = totalPrice + item.product_id.price * item.quantity;
            return null;
        });
        return totalPrice;
    }
    calDeliveryCharges = () => {
        let total = this.calcTotalPrice(this.state.cart);
        if (total > 1000) {
            return 0;
        }
        else {
            return (100 + total * 0.2);
        }
    }

    updateAddress = (e) => {
        let addressType = e.target.value;
        if (addressType === "default") {
            this.setState({
                AddressObject: {
                    addressLine1: '',
                    addressLine2: '',
                    street: '',
                    locality: '',
                    city: '',
                    state: '',
                    postalCode: '',
                    country: '',
                    addressType: ''
                },
            })
            return;
        }
        this.state.Address.map((address) => {
            if (address.addressType === addressType) {
                this.setState({
                    AddressObject: address,
                });
            }
            return null;
        })
    }

    renderCart = (cart) => {
        const cartUI = cart.map((item, key) => {
            const url = `/product/${item.product_id.id}`;
            const name = item.product_id.productName.substring(0, 25);
            return <p key={key}>
                <Link to={url} className="w-50 d-inline-block"> {name}..</Link>
                <this.state.tagName className="quantity ml-5">{item.quantity}</this.state.tagName>
                <span className="price">${item.product_id.price * item.quantity}</span>
            </p>
        });
        return cartUI;
    }

    renderAddressDropdown = () => {
        const dropdown = this.state.Address.map((address, key) => {
            return <option value={address.addressType} key={key}> {address.addressType}</option>;
        })
        return dropdown;
    }
    render() {
        return (
            <>
                <div className="container mt-2">
                    <div className="row">
                        <div className="checkout-form col-md-8 col-sm-12">
                            <form onSubmit={this.handleSubmit}>
                                <div className="card p-0 mr-2">
                                    <h3 className="card-header">Billing Address</h3>
                                    <div className="card-body">
                                        <div className="row">
                                            {this.state.Address ?
                                                <div className="form-group col-6">
                                                    <label htmlFor="addressDropdown">Select from previously stored address</label>
                                                    <select className="form-control" id="addressDropdown" onChange={(e) => this.updateAddress(e)}>
                                                        <option value="default">Select any one from dropdown</option>
                                                        {this.renderAddressDropdown()}
                                                    </select>
                                                </div>
                                                : ''
                                            }
                                            <div className="form-group col-6">
                                                <label htmlFor="addressType">Address Type</label>
                                                <input type="text" className="form-control" name="addressType" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.addressType} />
                                                {this.state.error.addressTypeError ? <span className="error-message">{this.state.error.addressTypeError}</span> : ""}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-group col-6">
                                                <label htmlFor="addressLine1">Address Line 1</label>
                                                <input type="text" className="form-control" name="addressLine1" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.addressLine1} />
                                                {this.state.error.addressLine1Error ? <span className="error-message">{this.state.error.addressLine1Error}</span> : ""}
                                            </div>
                                            <div className="form-group col-6">
                                                <label htmlFor="addressLine2">Address Line 2</label>
                                                <input type="text" className="form-control" id="addressLine2" name="addressLine2" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.addressLine2} />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-group col-6">
                                                <label htmlFor="street">street </label>
                                                <input type="text" className="form-control" id="street" name="street" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.street} />
                                                {this.state.error.streetError ? <span className="error-message">{this.state.error.streetError}</span> : ""}
                                            </div>
                                            <div className="form-group col-6">
                                                <label htmlFor="locality">locality </label>
                                                <input type="text" className="form-control" id="locality" name="locality" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.locality} />
                                                {this.state.error.localityError ? <span className="error-message">{this.state.error.localityError}</span> : ""}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-group col-6">
                                                <label htmlFor="city">city </label>
                                                <input type="text" className="form-control" id="city" name="city" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.city} />
                                                {this.state.error.cityError ? <span className="error-message">{this.state.error.cityError}</span> : ""}
                                            </div>
                                            <div className="form-group col-6">
                                                <label htmlFor="state">state </label>
                                                <input type="text" className="form-control" id="state" name="state" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.state} />
                                                {this.state.error.stateError ? <span className="error-message">{this.state.error.stateError}</span> : ""}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-group col-6">
                                                <label htmlFor="postal-code">Postal Code </label>
                                                <input type="number" className="form-control" id="postal-code" name="postalCode" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.postalCode} />
                                                {this.state.error.postalCodeError ? <span className="error-message">{this.state.error.postalCodeError}</span> : ""}
                                            </div>
                                            <div className="form-group col-6">
                                                <label htmlFor="country">country </label>
                                                <input type="text" className="form-control" id="country" name="country" onChange={(e) => this.handleChange(e, 'addressForm')} value={this.state.AddressObject.country} />
                                                {this.state.error.countryError ? <span className="error-message">{this.state.error.countryError}</span> : ""}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" value="Continue to checkout" className="btn-checkout-submit" />
                            </form>
                        </div>
                        <div className="col-md-4 col-sm-12">
                            <div className="card p-0">
                                <h4 className="card-header">Cart <span className="price"><i className="fa fa-shopping-cart"></i> <b>{this.calcTotalItems(this.state.cart)}</b></span></h4>
                                <div className="card-body">
                                    {this.renderCart(this.state.cart)}
                                    <p className="my-2">
                                        Delivery Charges
                                    <span className="price">${this.calDeliveryCharges()}</span>
                                    </p>
                                    <hr />
                                    <p className="my-1">Total <span className="price"><b>${this.calcTotalPrice(this.state.cart)}</b></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
function mapstateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn })
}
export default connect(
    mapstateToProps)(
        Checkout
    );