import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';

export const getCartItems = (userId) => {
    let obj = {
        user_id: userId,
    }
    let url = `${BASEURL}cart`;
    return axios.post(url, obj);
}