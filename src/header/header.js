import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import "../header/header.css"

class Header extends Component {
    logout = () => {
        window.localStorage.removeItem('user');
        window.localStorage.removeItem('isLoggedIn');
    }

    render() {
        let user = this.props.user.length > 0 ? JSON.parse(this.props.user) : {};
        user.accountType = user ? user.accountType : null;
        return (
            <>
                {
                    (user.accountType === 'Seller') ?
                        <div id="header" className="purchaser-header">
                            <nav className="navbar navbar-expand-lg navbar-dark bg-dark ">
                                <a className="navbar-brand" href="/">E-commerce</a>
                                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="navbar-toggler-icon"></span>
                                </button>
                                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul className="navbar-nav mr-auto">
                                        <li className="nav-item">
                                            <Link className="nav-link" to="/product">Product</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to="/add-product">Add Product</Link>
                                        </li>
                                    </ul>
                                    <form className="form-inline my-2 my-lg-0">
                                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                                        <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                                    </form>
                                    <ul className="navbar-nav">
                                        {this.props.isLoggedIn ?
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Profile
                                                </a>
                                                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                                    <a className="dropdown-item" href="/profile">Edit Profile</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item" href="/" onClick={this.logout}>LogOut</a>
                                                </div>
                                            </li>
                                            :
                                            <li className="nav-item">
                                                <a className="nav-link" href="/login">Login</a>
                                            </li>
                                        }
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        :
                        <div id="header" className="seller-header">
                            <nav className="navbar navbar-expand-lg navbar-dark bg-dark ">
                                <a className="navbar-brand" href="/">E-commerce</a>
                                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="navbar-toggler-icon"></span>
                                </button>
                                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                    <form className="form-inline my-2 my-lg-0 ml-lg-auto">
                                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                                        <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                                    </form>
                                    <ul className="navbar-nav">
                                        <li className="nav-item">
                                            <Link className="nav-link" to="/order">Orders</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to="/cart">Cart<i className="fa fa-shopping-cart text-white ml-1"></i></Link>
                                        </li>
                                        {this.props.isLoggedIn ?
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Profile
                                                </a>
                                                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                                    <a className="dropdown-item" href="/profile">Edit Profile</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item" href="/" onClick={this.logout}>LogOut</a>
                                                </div>
                                            </li>
                                            :
                                            <li className="nav-item">
                                                <a className="nav-link" href="/login">Login</a>
                                            </li>
                                        }
                                    </ul>

                                </div>
                            </nav>
                        </div>
                }
            </>
        )
    }
}
function mapStateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn })
}
export default connect(
    mapStateToProps)(
        Header
    );