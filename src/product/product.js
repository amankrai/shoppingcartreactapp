import React, {
    Component
} from 'react';
import '../product/product.css';
import { getProduct, placeOrder, getComments, addComment } from './productServices';
import { connect } from 'react-redux'
import { Link } from "react-router-dom";
import SubHeader from '../subHeader/subHeader';

// import ReactDOM from 'react-dom';

class Product extends Component {
    constructor(props) {
        super(props);
        let user = window.localStorage.getItem('user');
        let userId = user ? JSON.parse(user).id : null;
        this.state = {
            user_id: userId,
            productId: this.props.match.params.id,
            product: {},
            imageToShow: 0,
            Quantity: 1,
            variant_index: 0,
            comments: [],
            productRating: {
                count: 0
            },
            ratingForm: {
                rating: 0,
                comment: ''
            },
        };
    }
    componentDidMount() {
        if (this.state.productId) {
            getProduct(this.state.productId).then((response) => {
                const product_id = response.data.results.id;
                if (response.status === 200) {
                    this.setState({
                        product: response.data.results,
                    });
                    getComments(product_id).then((response) => {
                        if (response.data.results) {
                            let count = 0;
                            let rating = 0;
                            response.data.results.map((comment, key) => {
                                if (comment.rating > 0) {
                                    count += 1;
                                    rating += comment.rating;
                                }
                                return null;
                            });
                            const finalRating = rating / count;
                            this.setState({
                                comments: response.data.results,
                                productRating:
                                {
                                    rating: finalRating,
                                    count: count
                                }
                            });
                        }
                    });
                }
            }).catch(function (error) {
                if (error.response) {
                    alert(error.response.data.error);
                }
            });
        }
    }

    addToCart = (id) => {
        if (this.state.user_id) {
            let cartObject = {
                product_id: id,
                deliveryStatus: 0,
                "user_id": this.state.user_id,
                "quantity": this.state.Quantity,
                "variant_id": this.state.product.variant.id,
                "variant_index": this.state.variant_index
            }
            placeOrder(cartObject).then((response) => {
                if (response.status === 200) {
                    this.setState({
                        showMessage: 'Your product have been sucessfully added to cart.',
                    });
                }
            }).catch((error) => {
                console.log(error.response.data.error)
            });
        }
        else {
            this.setState({
                showMessage: 'Please login to add items in cart',
            });
        }
    }

    updateQuantity = (e) => {
        let value = parseInt(e.target.value);
        this.setState({
            Quantity: value,
        })
    }

    updateVariant = (e) => {
        const value = parseInt(e.target.value);
        this.setState({
            variant_index: value
        })
    }
    changeImage = (selectedImage) => {
        this.setState({
            ...this.state,
            imageToShow: selectedImage,
        });
    }

    modifyStars = (value) => {
        let collection = document.getElementById('rating-stars').children;
        for (let i = 0; i < collection.length; i++) {
            if (i < value) {
                collection[i].classList.add("checked");
            }
            else {
                collection[i].classList.remove("checked");
            }
        }
        this.setState({
            ratingForm: {
                ...this.state.ratingForm,
                rating: value
            }
        })
    }

    handleChange = (event) => {
        this.setState({
            ratingForm: {
                ...this.state.ratingForm,
                comment: event.target.value
            }
        });
    }

    addComment = () => {
        const obj = {
            data: this.state.ratingForm.comment,
            rating: this.state.ratingForm.rating,
            product_id: this.state.product.id,
            user_id: this.state.user_id
        }
        addComment(obj).then((response) => {
            if (response.data.message) {
                let tempComments = this.state.comments;
                if (response.data.comment) {
                    tempComments.push(response.data.comment);
                }
                this.setState({
                    showMessage: response.data.message,
                    comments: tempComments
                });
            }
        }).catch((err) => {
            console.log(err.data.error);
        })
    }
    renderDescription = (ProductDescription) => {
        ProductDescription = ProductDescription && ProductDescription.split("|");
        const descriptionUI = ProductDescription.map((description, key) => {
            return <div className="description" key={key}>{description}</div>
        });
        return descriptionUI;
    }
    renderThumnails = (images) => {
        let thumnailsUI = [];
        for (let i = 0; i < images.length; i++) {
            thumnailsUI.push(<div className="thumbnail-image text-center m-2" onClick={() => this.changeImage(i)} key={i}><img src={images[i].data} alt="card" /></div>);
        }
        return thumnailsUI;
    }
    renderImages = (images) => {
        let imagesUI = [];
        for (let i = 0; i < images.length; i++) {
            if (this.state.imageToShow === i) {
                imagesUI.push(<div className="d-show" key={i}><img src={images[i].data} alt="card" /></div>);
            }
            else {
                imagesUI.push(<div className="d-none" key={i}><img src={images[i].data} alt="card" /></div>);
            }
        }
        return imagesUI;
    }
    renderRating = () => {
        let rating = this.state.productRating.rating;
        rating = rating - Math.floor(rating) > 0.5 ? Math.ceil(rating) : Math.floor(rating);
        const ratingStars = [];
        if (rating) {
            for (let i = 0; i < rating; i++) {
                ratingStars.push(<span key={i} className="fa fa-star checked"></span>);
            }
            for (let i = rating; i < 5; i++) {
                ratingStars.push(<span key={i} className="fa fa-star"></span>);
            }
        }
        return ratingStars;
    }

    renderReview = () => {
        if (this.state.comments) {
            const reviewUI = this.state.comments.map((comment, key) => {
                const d = new Date(comment.createdAt);
                return (
                    <div className="review-div mt-3" key={key} >
                        <div>
                            {comment.rating > 0 ? <span className="rating mr-2 text-white"><i className="fa fa-star mr-2"></i>{comment.rating}</span> : ''}
                            <span className="review">{comment.data}</span>
                        </div>
                        <div className="mt-1 mb-2">{comment.user_id.name}
                            <span className="time float-right">
                                {d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear()}
                            </span>
                        </div>
                    </div>
                );
            });

            return reviewUI;
        }
        return null;
    }
    renderVariants = () => {
        const variants = JSON.parse(this.state.product.variant.variant);
        const UI = variants.map((variant, key) => {
            return (
                <div className="d-inline" key={key}>
                    <input type="radio" name="variant" id={variant.name} value={key} onChange={this.updateVariant} />
                    <label htmlFor={variant.name}>
                        {variant.name}
                    </label>
                </div>
            );
        });
        return UI;
    }

    render() {
        const product = this.state.product;
        const categoryUrl = "/category/" + (product.category_id ? product.category_id.id : null);
        const subCategoryUrl = "/subcategory/" + (product.subCategory_id ? product.subCategory_id.id : null);
        return (
            <>
                {
                    this.state.showMessage ?
                        <div className="alert alert-success text-center" role="alert">
                            {this.state.showMessage}
                        </div>
                        :
                        ''
                }
                <SubHeader />
                <div id="product-full" className="container d-flex mt-3">
                    <div className="left-container">
                        <div className="image-block d-inline-flex">
                            <div className="image-thumbnails mr-4">
                                {product.images && this.renderThumnails(product.images)}
                            </div>
                            <div className="image" ref="images">
                                {product.images && this.renderImages(product.images)}
                                {/* <img src={product.ProductImage && product.ProductImage.url} alt="Card cap" /> */}
                            </div>
                        </div>
                    </div>
                    <div className="right-container">
                        <Link to={categoryUrl}>
                            <div className="category d-inline-block mr-3 badge badge-info">{product.category_id && product.category_id.name}</div>
                        </Link>
                        <Link to={subCategoryUrl}>
                            <div className="sub-category d-inline-block badge badge-info">{product.subCategory_id && product.subCategory_id.name}</div>
                        </Link>
                        <div className="product-name">{product.ProductName && product.ProductName}</div>
                        <div className="product-price">Price: &nbsp;<b>$ {product.price && product.price}</b></div>
                        <div className="product-description">
                            Highlights:
                            {product.productDescription && this.renderDescription(product.productDescription)}
                        </div>
                        <div className="product-SellerId">
                            <span>Seller: </span>
                            <div className="seller-details d-inline-block">{product.seller && product.seller}</div>
                        </div>
                        <div className="rating">
                            {this.renderRating()}
                            {
                                this.state.productRating.count > 0 ?
                                    <span>&nbsp;{this.state.productRating.count} Ratings</span> :
                                    <span>Not yet rated</span>
                            }
                        </div>
                        <div className="add-to-cart">
                            <div className="Quantity d-inline-block mr-2">
                                <label htmlFor="Quantity">Quantity</label>
                                <input type="number" className="form-control" id="Quantity" name="Quantity" min="1" value={this.state.Quantity} onChange={this.updateQuantity} max="5" />
                            </div>
                            <div className="variant d-inline-block mr-2">
                                {product.variant ? this.renderVariants() : null}
                            </div>
                            <button type="button" className="d-block mt-2 btn btn-primary" onClick={() => this.addToCart(product.id)} >Add To Cart </button>
                        </div>
                        <div className="review-section mt-5">
                            <h3 className="d-inline-block">Ratings & Review</h3>
                            <div className="rating d-inline-block mx-4">
                                {this.renderRating()}
                                <span>&nbsp;{this.state.productRating.count} Ratings & {this.state.comments.length} Reviews</span>
                                <div className="modal fade" id="ratingFormModal">
                                    <div className="modal-dialog modal-xl">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h4 className="modal-title">Rate this product</h4>
                                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div className="modal-body">
                                                <div className="rating">
                                                    <span>Rate:</span>
                                                    <ul id="rating-stars" className="mx-2 d-inline-block">
                                                        <li className="d-inline-block" onClick={() => this.modifyStars(1)}><i className="fa fa-star mr-2"></i></li>
                                                        <li className="d-inline-block" onClick={() => this.modifyStars(2)}><i className="fa fa-star mr-2"></i></li>
                                                        <li className="d-inline-block" onClick={() => this.modifyStars(3)}><i className="fa fa-star mr-2"></i></li>
                                                        <li className="d-inline-block" onClick={() => this.modifyStars(4)}><i className="fa fa-star mr-2"></i></li>
                                                        <li className="d-inline-block" onClick={() => this.modifyStars(5)}><i className="fa fa-star mr-2"></i></li>
                                                    </ul>
                                                </div>
                                                <div className="review">
                                                    <div className="form-group">
                                                        <label htmlFor="data">Comment</label>
                                                        <textarea className="form-control" id="data" rows="3" placeholder="Type your comment here.." value={this.state.ratingForm.comment} onChange={this.handleChange} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.addComment}>Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="d-inline-block float-right">
                                <button className="btn btn-primary" data-toggle="modal" data-target="#ratingFormModal">Rate Product</button>
                            </div>
                            <div className="review-box">{this.renderReview()}</div>
                        </div>
                    </div>
                </div>

            </>
        );
    }
}
function mapStateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn })
}
export default connect(
    mapStateToProps)(
        Product
    );