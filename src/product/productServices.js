import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';
export const getProduct = (productId) => {
    let url = `${BASEURL}product/${productId}`;
    return axios.get(url);
}

export const placeOrder = (object) => {
    let url = `${BASEURL}add-to-cart`;
    // let headers = HEADERS;
    return axios.post(url, object);
}
export const getComments = (product_id) => {
    let url = `${BASEURL}comment/${product_id}`;
    // let headers = HEADERS;
    return axios.get(url);
}
export const addComment = (obj) => {
    let url = `${BASEURL}comment`;
    // let headers = HEADERS;
    return axios.post(url, obj);
}