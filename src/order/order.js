import React, {
    Component
} from 'react';
import '../order/order.css';
import { connect } from 'react-redux'
import { getOrders, getProduct } from './orderServices';
import { Link } from "react-router-dom";

class Order extends Component {
    constructor(props) {
        super(props);
        let userID = this.props.user.length > 0 ? JSON.parse(this.props.user)['id'] : null;
        this.state = {
            orders: [],
            userId: userID,

        }
    }
    componentWillMount() {
        getOrders(this.state.userId)
            .then(async (response) => {
                console.log(response)
                let data = response.data;
                if (response.status === 200) {
                    data.results.map(async (order, key) => {
                        const dataa = Object.values(order.data).map(async (product) => {
                            await getProduct(product.product_id).then((response) => {
                                product.productValue = response.data.results
                            });
                        });
                        await Promise.all(dataa);
                        this.setState({
                            orders: data.results,
                        }, () => {
                            console.log("statess:", Object.entries(this.state.orders[0].data[0]));
                        });
                    });
                }
            })
            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });
    }
    getProductUi = (products) => {
        const list = Object.values(products).map((product, key) => {
            if (product.productValue) {
                const name = product.productValue.productName;
                return (<><span key={key}>{name} * {product.quantity}</span><br /></>)
            }
            return null;
        });
        return list;
    }

    renderOrder = () => {
        console.log("real", this.state.orders[0]);
        const ordersUi = this.state.orders.map((order, key) => {
            const url = '/orderReview/' + order.id;
            return (
                <tr key={key}>
                    <th scope="row">{key + 1}</th>
                    <td>{order.id}</td>
                    <td>{this.getProductUi(order.data)}</td>
                    <td>{Date(order.createdAt)}</td>
                    {order.Delivered ? <td>Your Order has been delivered</td> : <td>Your order is on the way.</td>}
                    <td><Link to={url} className="btn btn-primary">Review Order</Link></td>
                </tr>
            );
        });
        return ordersUi
    }

    render() {
        return (
            this.state.orders.length > 0 ?
                <div className="container mt-3">
                    <h2 className="mb-4">Your Orders</h2>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col" width="20px">#</th>
                                <th scope="col">Order Id</th>
                                <th scope="col">Products</th>
                                <th scope="col">Order Date</th>
                                <th scope="col">Delivered</th>
                                <th scope="col" width="100px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderOrder()}
                        </tbody>
                    </table>
                </div>
                :
                <div className="jumbotron text-center">
                    <h2>You don't have any orders yet.</h2>
                </div>
        );
    }
}
function mapStateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn })
}
export default connect(
    mapStateToProps)(
        Order
    );