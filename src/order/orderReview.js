import React, {
    Component
} from 'react';
import '../order/orderReview.css';
import { getOrderDetail } from './orderServices';
class OrderReview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orderId: this.props.match.params.id,
            order: [],
            requestSent: false,
            products: []
        }
    }
    componentDidMount() {
        getOrderDetail(this.state.orderId)
            .then((response) => {
                let data = response.data;
                console.log(data)
                if (response.status === 200) {
                    this.setState({
                        ...this.state,
                        order: data.results,
                    });
                }
            })
            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });
    }

    renderProduct = () => {
        if (this.state.order.data) {
            const productListUi = Object.values(this.state.order.data).map((product, key) => {
                return <tr height="40px" key={key}>
                    <td>{key + 1}</td>
                    <td>
                        <img className="" src={product.productValue.images[0].data} alt="Card cap" height="100px" />
                    </td>
                    <td>{product.productValue.productName}</td>
                    <td><b>&#x20B9; {product.productValue.price}</b></td>
                    <td>{product.quantity}</td>
                </tr>
            });
            return productListUi;
        }
    }

    render() {
        return (
            <>
                <div className="container">
                    <h4 className="px-3 py-4">Order No: {this.state.orderId} </h4>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">ProductImage</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quanity</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderProduct()}
                        </tbody>
                    </table>
                </div>
            </>
        );
    }
}
export default OrderReview;