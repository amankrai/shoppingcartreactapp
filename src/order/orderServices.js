import {
    BASEURL
} from '../constant/constant';
import axios from 'axios';

export const getOrders = (userId) => {
    let url = `${BASEURL}get-order/${userId}`;
    return axios.get(url);
}

export const getOrderDetail = (orderId) => {
    let url = `${BASEURL}get-order-details/${orderId}`;
    return axios.get(url);
}

export const getProduct = (id) => {
    let url = `${BASEURL}product/${id}`;
    return axios.get(url);
}
