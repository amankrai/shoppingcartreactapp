import React, { Component } from 'react';
// import { Redirect } from 'react-router';
import { getCategories } from '../filters/filtersServices';
import '../filters/filters.css';

class Filters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            appliedFilters: [
                { filterType: "price", name: "min-price", value: 0 },
                { filterType: "price", name: "max-price", value: 100000 },
            ],
            subCategories: [],
            minPrice: 0,
            maxPrice: 100000,
        };
    }
    componentWillMount() {
        getCategories().then((response) => {
            if (response.data) {
                this.setState({
                    categories: response.data.results,
                });
            }
        });
    }

    handleInputChange = (event, filterType) => {
        const target = event.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let temp = this.state.appliedFilters;
        let filterObject = { filterType: filterType, id: target.id, value: value };
        if (filterType === "category") {
            if (value) {
                temp.push(filterObject)
            }
            else {
                let index = -1;
                for (let i = 0; i < temp.length; i++) {
                    if (temp[i].filterType === "category") {
                        delete temp[i].value;
                        temp[i].id = temp[i].id + "";
                        delete filterObject.value;
                        if (JSON.stringify(temp[i]) === JSON.stringify(filterObject)) {

                            index = i;
                        }
                    }
                }
                if (index > -1) {
                    temp.splice(index, 1);
                }
            }
        }
        else if (filterType === "price") {
            let minValue = this.state.minPrice;
            let maxValue = this.state.maxPrice;
            value = parseInt(value);
            if (name === "min-price") {
                this.setState({ minPrice: value });
                minValue = value;
            }
            if (name === "max-price") {
                this.setState({ maxPrice: value })
                maxValue = value;
            }
            for (let i = 0; i < temp.length; i++) {
                if (temp[i].name === "min-price") {
                    temp[i].value = minValue;
                }
                if (temp[i].name === "max-price") {
                    temp[i].value = maxValue;
                }
            }
        }
        this.setState({
            appliedFilters: temp
        });
        this.props.updateFilter(this.state.appliedFilters);

    }

    renderCategoryFilters = () => {
        const UI = this.state.categories.map((category, key) => {
            return <div className="form-check p-0 my-1" key={key}>
                <label className="d-block main" htmlFor={category.id}>
                    {category.name}
                    <input className="form-check-input" type="checkbox" name={category.name} id={category.id} checked={this.state.isGoing} onChange={(e) => this.handleInputChange(e, 'category')} />
                    <span className="geekmark"></span>
                </label>
            </div>;
        });
        return UI;
    }

    render() {
        return <>
            <div className="price-filters col-10">
                <button className="btn btn-primary w-100 my-2" type="button" data-toggle="collapse" data-target="#price-filter" aria-expanded="false" aria-controls="collapseExample">
                    Price
                    </button>
                <div className="collapse" id="price-filter">
                    <div className="form-group d-flex justify-content-between align-items-center m-0 my-2">
                        <label htmlFor="min-price">Min Price</label>
                        <input type="number" className="form-control w-50" id="min-price" name="min-price" min="0" max="99999" value={this.state.minPrice} onChange={(e) => this.handleInputChange(e, 'price')} />
                    </div>
                    <div className="form-group d-flex justify-content-between align-items-center m-0 my-2">
                        <label htmlFor="max-price ">Max Price</label>
                        <input type="number" className="form-control w-50" id="max-price" name="max-price" min="1" max="100000" value={this.state.maxPrice} onChange={(e) => this.handleInputChange(e, 'price')} />
                    </div>
                </div>
            </div>
            <div className="category-filters col-10">
                <button className="btn btn-primary w-100 my-2" type="button" data-toggle="collapse" data-target="#category-filter" aria-expanded="false" aria-controls="collapseExample">
                    Category
                    </button>
                <div className="collapse" id="category-filter">
                    {this.renderCategoryFilters()}
                </div>
            </div>
        </>
    }
}

export default Filters;