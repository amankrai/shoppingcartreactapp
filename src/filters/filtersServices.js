import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';
export const getCategories = () => {
    let url = `${BASEURL}category`;
    return axios.get(url)
}