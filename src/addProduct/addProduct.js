import React, {
    Component
} from 'react';
import { Redirect } from 'react-router';
import '../home/home.css';
import { connect } from 'react-redux';
import { getCategories, getSubCategories, addProduct, getBrand } from '../addProduct/addProductServices';


class AddProduct extends Component {
    constructor(props) {
        super(props);
        this.ProductImage = React.createRef();
        this.encodeImageFileAsURL = this.encodeImageFileAsURL.bind(this);
        let userID = this.props.user.length > 0 ? JSON.parse(this.props.user)['id'] : null;
        this.state = {
            categories: [],
            subCategories: [],
            brands: [],
            showCategory: true,
            showSubCategory: true,
            showBrand: true,
            product: {
                productName: '',
                productDescription: '',
                available: 0,
                category: 0,
                subCategory: 0,
                userId: userID,
                image: '',
                price: 0,
            }
        }
    }

    componentWillMount() {
        getCategories().then((response) => {
            if (response.data) {
                let data = response.data.results;
                this.setState({
                    categories: data,
                });
            }
        });

        getBrand().then((response) => {
            if (response.data) {
                let data = response.data.results;
                this.setState({
                    brands: data,
                });
            }
        });
    }

    handleChange = (event, type) => {
        let name = event.target.name;
        let value = event.target.value;
        if (type === "category" || type === "subCategory" || type === "brand") {
            if (value === "add-new") {
                if (type === "category") {
                    this.setState({
                        showCategory: false,
                        product: {
                            ...this.state.product,
                            category: ''
                        }
                    })
                }
                else if (type === "subCategory") {
                    this.setState({
                        showSubCategory: false,
                        product: {
                            ...this.state.product,
                            subCategory: ''
                        }
                    })
                }
                else if (type === "brand") {
                    console.log('er', type)
                    this.setState({
                        showBrand: false,
                        product: {
                            ...this.state.product,
                            brand: ''
                        }
                    })
                }
                return;
            } else {
                const val = parseInt(value);
                if (type === "category") {
                    getSubCategories(val).then((response) => {
                        if (response.data) {
                            let data = response.data.results;
                            this.setState({
                                subCategories: data,
                            });
                        }
                    });
                }

                this.setState({
                    product: {
                        ...this.state.product,
                        [type]: val
                    }
                });
                return;
            }
        }
        this.setState({
            product: {
                ...this.state.product,
                [name]: value
            }
        })
    }

    encodeImageFileAsURL() {
        let file = this.ProductImage.current.files[0];
        let reader = new FileReader();
        let base64Image;
        reader.readAsDataURL(file);
        reader.onloadend = () => {
            base64Image = reader.result;
            this.setState({
                product: {
                    ...this.state.product,
                    image: base64Image
                }
            })
        }
        return;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        addProduct(this.state.product).then((response) => {
            if (response.status === 200) {
                alert('Your Product has been added successfully');
                window.location.href = "/";
            }
        }).catch(function (error) {
            if (error.response) {
                console.log(error.response.data.error);
            }
        });
    }

    renderDatalist = (list) => {
        const UI = list.map((item, key) => {
            return <option value={item.id} key={key}>{item.name}</option>
        })
        return UI;
    }
    render() {
        console.log(this.state)
        return <>
            {this.props.isLoggedIn ?
                <form id="add-product" className="container" onSubmit={(e) => this.handleSubmit(e)}>
                    <h2 className="text-center jumbotron">Add Product</h2>
                    <div className="form-group">
                        <label htmlFor="productName">Product Name</label>
                        <input type="text" className="form-control" id="productName" name="productName" onChange={(e) => this.handleChange(e)} value={this.state.product.productName} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="productDescription">Product Description</label>
                        <textarea className="form-control" id="productDescription" rows="3" name="productDescription" onChange={(e) => this.handleChange(e)} value={this.state.product.productDescription}></textarea>
                    </div>
                    <div className="form-group">
                        <label htmlFor="available">How many products in stock</label>
                        <input type="number" className="form-control" id="available" name="available" onChange={(e) => this.handleChange(e)} value={this.state.product.available} />
                    </div>
                    <div className="row">
                        <div className="form-group col-4">
                            <label htmlFor="category">Category</label>
                            {this.state.showCategory ?
                                <select className="form-control" value={this.state.product.category} id="category" onChange={(e) => this.handleChange(e, 'category')}>
                                    <option value='0'>---SELECT---</option>
                                    {this.renderDatalist(this.state.categories)}
                                    <option value='add-new'>Add new category</option>
                                </select>
                                :
                                <input type="text" className="form-control" id="category" name="category" placeholder="Type your category" onChange={(e) => this.handleChange(e)} value={this.state.product.category} />
                            }

                        </div>
                        <div className="form-group col-4">
                            <label htmlFor="subCategory">Sub Category</label>
                            {this.state.showSubCategory ?
                                <select className="form-control" value={this.state.product.subCategory} id="subCategory" onChange={(e) => this.handleChange(e, 'subCategory')}>
                                    <option value='0'>---SELECT---</option>
                                    {this.renderDatalist(this.state.subCategories)}
                                    <option value='add-new'>Add new sub category</option>
                                </select>
                                :
                                <input type="text" className="form-control" id="subCategory" name="subCategory" placeholder="Type your Sub category" onChange={(e) => this.handleChange(e)} value={this.state.product.subCategory} />
                            }
                        </div>
                        <div className="form-group col-4">
                            <label htmlFor="brand">Brand</label>
                            {this.state.showBrand ?
                                <select className="form-control" value={this.state.product.brand} id="brand" onChange={(e) => this.handleChange(e, 'brand')}>
                                    <option value='0'>---SELECT---</option>
                                    {this.renderDatalist(this.state.brands)}
                                    <option value='add-new'>Add new Brand</option>
                                </select>
                                :
                                <input type="text" className="form-control" id="brand" name="brand" placeholder="Type your Bran name" onChange={(e) => this.handleChange(e)} value={this.state.product.brand} />
                            }
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-5">
                            <label htmlFor="price">Price</label>
                            <input type="number" className="form-control" id="price" name="price" onChange={(e) => this.handleChange(e)} value={this.state.product.price} />
                        </div>
                        <div className="form-group col-5">
                            <label htmlFor="ProductImage">Upload Product Image</label>
                            <input type="file" className="form-control-file" id="ProductImage" name="ProductImage" ref={this.ProductImage} onChange={(e) => this.encodeImageFileAsURL(e)} />
                        </div>
                        <div className="col-2">
                            <button type="submit" className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                        </div>
                    </div>
                </form>
                : <Redirect to='/login' />
            }
        </>
    }
}
function mapStateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn })
}
export default connect(
    mapStateToProps)(
        AddProduct
    );