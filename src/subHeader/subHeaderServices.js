import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';
export const getCategories = () => {
    let url = `${BASEURL}category`;
    return axios.get(url)
}
export const getSubCategories = (id) => {
    let url = `${BASEURL}subCategory/${id}`;
    return axios.get(url);
}