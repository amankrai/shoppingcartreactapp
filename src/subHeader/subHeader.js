import React, {
    Component
} from 'react';
import '../subHeader/subHeader.css';
import { Link } from "react-router-dom";
import { getCategories, getSubCategories } from './subHeaderServices.js';

class SubHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            // subCategories: []
        }
    }
    componentDidMount() {
        getCategories().then(async (response) => {
            if (response.data) {
                let categories = response.data.results;
                const categoryhandle = await categories.map(async (category, key) => {
                    await getSubCategories(category.id).then((response) => {
                        if (response.data) {
                            categories[key].subCategory = response.data.results;
                        }
                    });
                });

                await Promise.all(categoryhandle);
                this.setState({
                    categories: categories
                });
            }
        });
    }
    renderCategories = () => {
        const UI = Object.values(this.state.categories).map((category, key) => {
            let url = "/category/" + category.id;
            return (
                <li key={key}>
                    <Link to={url}><span className="pr-2">{category.name}</span><i className="fa fa-angle-down" aria-hidden="true"></i></Link>
                    {
                        category['subCategory'] ?
                            <ul className="menu-level-1 bg-white">
                                {
                                    category.subCategory.map((subCategory, key) => {
                                        let url = "/subcategory/" + subCategory.id;
                                        return (<li key={key}><Link to={url}>{subCategory.name}</Link></li>);
                                    })
                                }
                            </ul>
                            :
                            null
                    }
                </li >
            );
        });
        return UI;
    }

    render() {
        return (
            <div className="sub-header">
                <div className="bg-white">
                    <ul className="menu-level-0 container">
                        {this.renderCategories()}
                    </ul>
                </div>
            </div>
        );
    }
}
export default SubHeader