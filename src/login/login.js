import React, { Component } from 'react';
import { loginUser } from './loginService';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import '../login/login.css';

// import { BrowserRouter as Router } from 'react-router-dom';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            password: '',
            verified: false,
            invalidUsername: false,
            invalidPassword: false,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit() {
        if (this.state.name === '') {
            this.setState({
                invalidUsername: true,
            });
            return;
        }
        if (this.state.password === '') {
            this.setState({
                invalidPassword: true,
            });
            return;
        }
        let userObj = {
            "userName": this.state.name,
            "password": this.state.password
        }
        loginUser(userObj)
            .then((response) => {
                let data = response.data;
                if (data.message || data.error) {
                    const message = data.message ? data.message : data.error;
                    this.setState({
                        message: message,
                    })
                }
                else if (data.user) {
                    window.localStorage.setItem('user', JSON.stringify(data.user));
                    window.localStorage.setItem('isLoggedIn', 'true');
                    this.props.updateLogin(JSON.stringify(data.user));
                    this.setState({
                        ...this.state,
                        verified: true
                    });
                }
            })
            .catch(function (error) {
                if (error.response) {
                    alert(error.response.data.error);
                }
            });
    }
    handleUserNameChange = (e) => {
        this.setState({ name: e.target.value });
    }
    handlePasswordChange = (e) => {
        this.setState({ password: e.target.value });
    }

    render() {
        return (this.state.verified ? <Redirect to='/' /> :
            <>
                {
                    this.state.message ?
                        <div className="alert alert-warning sticky-top" role="alert">
                            {this.state.message}
                        </div>
                        :
                        ''
                }
                <div className="limiter">
                    <div className="container-login100">
                        <div className="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
                            <form className="login100-form validate-form">
                                <span className="login100-form-title p-b-49">
                                    Login
					            </span>
                                <div className="wrap-input100 validate-input m-b-23" data-validate="Username is reauired">
                                    <span className="label-input100">Username</span>
                                    <input type="text" className="input100" id="username" value={this.state.name} onChange={this.handleUserNameChange} placeholder="Type your username" />
                                    <span className="focus-input100" data-symbol="&#xf206;"></span>
                                </div>
                                <div className="wrap-input100 validate-input" data-validate="Password is required">
                                    <span className="label-input100">Password</span>
                                    <input type="password" className="input100" id="pwd" value={this.state.password} onChange={this.handlePasswordChange} placeholder="Type your password" />
                                    <span className="focus-input100" data-symbol="&#xf190;"></span>
                                </div>
                                <div className="text-right p-t-8 p-b-31">
                                    <a href="/forgot-password">
                                        Forgot password?
						            </a>
                                </div>
                                <div className="container-login100-form-btn">
                                    <div className="wrap-login100-form-btn">
                                        <div className="login100-form-bgbtn"></div>
                                        <button type="button" className="login100-form-btn" onClick={this.handleSubmit}>Login</button>
                                    </div>
                                </div>
                                <div className="txt1 text-center p-t-30 p-b-20">
                                    <span>Or Sign Up Using</span>
                                </div>

                                <div className="flex-c-m">
                                    <a href="www.facebook.com" className="login100-social-item bg1">
                                        <i className="fa fa-facebook"></i>
                                    </a>

                                    <a href="www.twitter.com" className="login100-social-item bg2">
                                        <i className="fa fa-twitter"></i>
                                    </a>

                                    <a href="www.google.com" className="login100-social-item bg3">
                                        <i className="fa fa-google"></i>
                                    </a>
                                </div>
                                <div className="flex-col-c p-t-20">
                                    <Link to="/signup" className="txt2">SignUp</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="dropDownSelect1"></div>
                <script src="vendor/animsition/js/animsition.min.js"></script>
                <script src="vendor/countdowntime/countdowntime.js"></script>
                <script src="js/main.js"></script>
            </>
            // <div id="login">
            //     <div className="form col-md-6 mx-auto mt-5">
            //         <h2>Login</h2>
            //         <form>
            //             <div className="form-group">
            //                 <label htmlFor="username">User Name:</label>
            //                 <input type="text" className="form-control" id="username" value={this.state.name} onChange={this.handleUserNameChange} />
            //                 {this.state.invalidUsername ? <span className="error-message">Please enter valid Username</span> : ""}
            //             </div>
            //             <div className="form-group">
            //                 <label htmlFor="pwd">Password:</label>
            //                 <input type="password" className="form-control" id="pwd" value={this.state.password} onChange={this.handlePasswordChange} />
            //                 {this.setState.invalidPassword ? <span className="error-message">Please enter valid Password</span> : ""}
            //             </div>
            //             <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
            //         </form>
            //     </div>
            //     <div><Link to="/signup">SignUp</Link></div>
            // </div>
        );
    }

}
function mapDispatchToProps(dispatch) {
    return (
        {
            updateLogin: (user) => { dispatch({ type: "LOG_IN", user: user }) }
        })
}

function mapStateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn })
}
export default connect(
    mapStateToProps, mapDispatchToProps)(
        Login
    );