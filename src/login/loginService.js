import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';

export const loginUser = (object) => {
    let url = `${BASEURL}login`;
    return axios.post(url, object);
}