import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';
export const createUser = (userObject) => {
    // let headers = HEADERS;
    let url = `${BASEURL}signup`;
    delete userObject.rePassword;
    return axios.post(url, userObject);
}