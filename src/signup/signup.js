import React, { Component } from 'react';
import '../signup/signup.css';
import { createUser } from './signupServices';
import { connect } from 'react-redux';
class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            registerObject: {
                name: '',
                userName: '',
                emailAddress: '',
                password: '',
                rePassword: '',
                accountType: 'Seller'
            },
            passwordMatch: null,
            errorMessage: null,
        }
    }
    handleChange = (e) => {
        let name = e.target.id;
        let value = e.target.accountType === 'checkbox' ? e.target.checked : e.target.value;
        if (name === "accountType") {
            if (value) {
                this.setState({
                    registerObject: {
                        ...this.state.registerObject,
                        accountType: 'Purchaser'
                    }
                });
            }
            else {
                this.setState({
                    registerObject: {
                        ...this.state.registerObject,
                        accountType: 'Seller'
                    }
                });
            }
            return;
        }
        let error = name + 'Error';
        this.setState({
            [error]: "",
            registerObject: {
                ...this.state.registerObject,
                [name]: value
            }
        });
        if (name === "rePassword") {
            if (value === "") {
                this.setState({
                    rePasswordError: "*Required Feild"
                });
                return;
            }
            if (this.state.registerObject.password === value) {
                this.setState({
                    passwordMatch: 1,
                })
            }
            else {
                this.setState({
                    passwordMatch: 0,
                })
            }
        }
    }
    validate = () => {
        const obj = this.state.registerObject;
        console.log(obj)
        let flag = false;
        if (obj.name === '') {
            this.setState({
                "NameError": "*Required Feild"
            });
            flag = true;
        }
        if (obj.userName === '') {
            this.setState({
                "usernameError": "*Required Feild"
            });
            flag = true;
        }
        if (obj.emailAddress === '') {
            this.setState({
                "emailError": "*Required Feild"
            });
            flag = true;
        }
        if (obj.password === '') {
            this.setState({
                "passwordError": "*Required Feild"
            });
            flag = true;
        }
        if (obj.name === '') {
            this.setState({
                "NameError": "*Required Feild"
            });
            flag = true;
        }
        return flag;
    }
    handleSubmit = (event) => {
        event.preventDefault();
        if (this.validate()) {
            return;
        }
        createUser(this.state.registerObject).then((response) => {
            console.log('res', response)
            let data = response.data;
            if (data.user.id) {
                // window.localStorage.setItem('user', JSON.stringify(data));
                // window.localStorage.setItem('isLoggedIn', 'true');
                // this.props.updateLogin(data.user);
                this.setState({
                    ...this.state,
                    verified: true
                });
                this.props.history.push('/login');
            }
        }).catch((error) => {
            this.setState({
                errorMessage: error.response.data.error
            });
        });
    }
    render() {
        return <>
            <div className="page-wrapper bg-blue p-t-10 p-b-100 font-robo">
                {this.state.errorMessage ?
                    <div className="alert sticky-top alert-danger alert-dismissible fade show" role="alert">
                        <strong>{this.state.errorMessage}</strong> Please Re submit after making required changes.
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    : ""}
                <div className="wrapper wrapper--w680">
                    <div className="card card-1">
                        <div className="card-heading"></div>
                        <div className="card-body">
                            <h2 className="title">Registration Info </h2>
                            <form onSubmit={this.handleSubmit}>
                                <div className="wrap-input100 validate-input m-b-23" data-validate="Name is required">
                                    <span className="label-input100">Name</span>
                                    <input type="text" className="input100" id="name" placeholder="Type your name" value={this.state.registerObject.name} onChange={this.handleChange} />
                                    <span className="focus-input100" data-symbol="&#xf207;"></span>
                                    {this.state.NameError ? <span className="error-message">{this.state.NameError}</span> : ""}
                                </div>
                                <div className="wrap-input100 validate-input m-b-23" data-validate="Username is reauired">
                                    <span className="label-input100">Username</span>
                                    <input type="text" className="input100" id="userName" placeholder="Type your username" value={this.state.registerObject.userName} onChange={this.handleChange} />
                                    <span className="focus-input100" data-symbol="&#xf206;"></span>
                                    {this.state.usernameError ? <span className="error-message">{this.state.usernameError}</span> : ""}
                                </div>
                                <div className="wrap-input100 validate-input m-b-23" data-validate="Email is required">
                                    <span className="label-input100">Email</span>
                                    <input type="email" className="input100" id="emailAddress" placeholder="Type your Email" value={this.state.registerObject.emailAddress} onChange={this.handleChange} />
                                    <span className="focus-input100" data-symbol="&#xf15a;"></span>
                                    {this.state.emailError ? <span className="error-message">{this.state.emailError}</span> : ""}

                                </div>
                                <div className="wrap-input100 validate-input m-b-23" data-validate="Password is required">
                                    <span className="label-input100">Password</span>
                                    <input type="password" className="input100" id="password" placeholder="Type your password" value={this.state.registerObject.password} onChange={this.handleChange} />
                                    <span className="focus-input100" data-symbol="&#xf190;"></span>
                                    {this.state.passwordError ? <span className="error-message">{this.state.passwordError}</span> : ""}

                                </div>
                                <div className="wrap-input100 validate-input m-b-23" data-validate="Re-enter Password is required">
                                    <span className="label-input100">Re-enter Password</span>
                                    <input type="password" className="input100" id="rePassword" placeholder="Type your password" value={this.state.registerObject.rePassword} onChange={this.handleChange} />
                                    <span className="focus-input100" data-symbol="&#xf190;"></span>
                                    {this.state.repasswordError ? <span className="error-message">{this.state.repasswordError}</span> : ""}
                                    {this.state.passwordMatch === 1 ? <span className="success-message">Password Match Succesfully</span> : (this.state.passwordMatch === 0 ? <span className="error-message">Password didn't match</span> : "")}
                                </div>
                                <div className="wrap-input100 m-b-23 border-0">
                                    <span className="label-input100">Select account type</span>
                                    <input className="tgl tgl-flip" id="accountType" type="checkbox" onChange={this.handleChange} />
                                    <label className="tgl-btn m-l-100" data-tg-off="Seller " data-tg-on="Purchaser" htmlFor="accountType"></label>
                                </div>
                                <div className="container-login100-form-btn">
                                    <div className="wrap-login100-form-btn">
                                        <div className="login100-form-bgbtn"></div>
                                        <button type="submit" className="login100-form-btn" >Signup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    }
}
function mapDispatchToProps(dispatch) {
    return ({
        updateLogin: (user) => { dispatch({ type: "LOG_IN", user: user }) }
    });
}

function mapStateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn });
}
export default connect(
    mapStateToProps, mapDispatchToProps)(
        Signup
    );