import React, {
    Component
} from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import '../home/home.css';
import Filters from '../filters/filters';
import Slider from './homeCarousel/homeSlider';
import { getProducts } from './homeService';
import { isNull } from 'util';
import SubHeader from '../subHeader/subHeader';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            waiting: true,
            appliedFilters: [],
        };
    }
    componentWillMount() {
        getProducts().then((response) => {
            let data = response.data.results;
            if (data) {
                this.setState({
                    products: data,
                    waiting: false
                });
            }
        })
            .catch(function (error) {
                alert(error);
            });
    }

    updateFilter = (filters) => {
        this.setState({
            appliedFilters: filters,
        });
    }
    checkProductToRender = (product) => {
        let filters = this.state.appliedFilters;
        let renderState = false;
        let categoryCount = 0;
        for (let i = 0; i < filters.length; i++) {
            if (filters[i].filterType === "category") {
                categoryCount = categoryCount + 1;
                filters[i].id = parseInt(filters[i].id);
                if (product.category_id === filters[i].id) {
                    renderState = renderState || true;
                }
                else {
                    renderState = renderState || false;
                }
            }
        }
        if (categoryCount === 0) {
            renderState = true;
        }
        for (let i = 0; i < filters.length; i++) {
            if (filters[i].filterType === "price") {
                let minValue = 0;
                let maxValue = 100000;
                for (let j = 0; j < filters.length; j++) {
                    if (filters[i].name === "min-price") {
                        if (filters[j].name === "max-price") {
                            minValue = filters[i].value;
                            maxValue = filters[j].value;
                        }
                    }
                    else {
                        if (filters[j].name === "min-price") {
                            minValue = filters[j].value;
                            maxValue = filters[i].value;
                        }
                    }
                }
                renderState = renderState && ((product.price <= maxValue) && (product.price >= minValue));
            }
        }
        return renderState;
    }

    renderProduct = () => {
        let productUi = this.state.products.map((product, key) => {
            let imageValue = product.images[0] ? product.images[0].data : null;
            let showProduct = this.checkProductToRender(product, this.state.appliedFilters);
            if (showProduct) {
                let productName = product.productName.substring(0, 23) + "...";
                let url = "/product/" + product.id;
                return <Link to={url} key={key}>
                    <div className="card mt-2 mr-1">
                        <div className="card-image">
                            <img className="card-img-top" src={imageValue} alt="Card cap" />
                        </div>
                        <div className="card-body">
                            <p className="card-title">{productName}</p>
                            <p className="card-text"><b>$ {product.price}</b></p>
                        </div>
                    </div>
                </Link>
            }
            return null;
        });
        if (productUi.every(isNull)) {
            productUi = <div className="jumbotron w-100 text-center">
                <img src={require('../images/noProduct.png')} alt="No Product to display" />
                <h3>Sorry, no results found!
                Please check the spelling or try searching for something else.
                </h3>
            </div>
        }
        return productUi;
    }
    render() {
        return (
            <>
                {this.state.waiting ? <div><h1>Test</h1></div> :
                    <>
                        <SubHeader />
                        <div className="d-flex">
                            <div className="order-2 w-75">
                                <Slider />
                                <div id="products" className="d-flex w-100 flex-wrap ml-lg-4">
                                    {this.renderProduct()}
                                </div>
                            </div>
                            <div className="order-1 filters">
                                <Filters updateFilter={this.updateFilter} />
                            </div>
                        </div>
                    </>
                }
            </>);
    }

}

function mapStateToProps(state) {
    return ({ user: state.user, isLoggedIn: state.isLoggedIn, filters: state.filters })
}
export default connect(
    mapStateToProps)(
        Home
    );