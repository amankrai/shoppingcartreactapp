import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../constant/constant';

export const getProducts = () => {
    let url = `${BASEURL}product`;
    return axios.get(url);
}