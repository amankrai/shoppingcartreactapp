import React, {
    Component
} from 'react';
import { Link } from "react-router-dom";
import '../homeCarousel/homeSlider.css';
import { getRecentProducts } from './homeSliderServices';

class Slider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        }
    }
    componentDidMount() {
        getRecentProducts().then((response) => {
            if (response.status === 200) {
                if (response.data.error) {
                    console.log(response.data.error);
                }
                else {
                    this.setState({
                        products: response.data.results
                    });
                }
            }
        }).catch((err) => {
            console.log(err.data)
        });
    }
    renderSlider = () => {
        const color = ['#ffe5a3', '#084802', '#02e6f0', '#FF9903', 'black'];
        const SliderUI = this.state.products.map((product, key) => {
            // const index = Math.floor(Math.random() * Math.floor(5));
            const style = {
                background: color[key],
            }
            const imageData = product.images[0].data;
            const url = `/product/${product.id}`;
            if (key === 0) {
                return (
                    <div className="carousel-item active" key={key} style={style}>
                        <Link to={url}><img src={imageData} alt={product.productName} width="1100" height="500" /></Link>
                        <Link to={url}>
                            <div className="carousel-caption text-white">
                                <h3>{product.brand_id.name}</h3>
                                <div className="bottom">
                                    <div>{product.productName}</div>
                                    <div>{product.productDescription}</div>
                                </div>
                            </div>
                        </Link>
                    </div>
                );
            }
            else {
                return (
                    <div className="carousel-item" key={key} style={style}>
                        <Link to={url}><img src={imageData} alt={product.productName} width="1100" height="500" /></Link>
                        <Link to={url}>
                            <div className="carousel-caption text-white">
                                <h3>{product.brand_id.name}</h3>
                                <div className="bottom">
                                    <div>{product.productName}</div>
                                    <div>{product.productDescription}</div>
                                </div>
                            </div>
                        </Link>
                    </div>
                );
            }

        });
        return SliderUI;
    }

    renderCarouselIndicators = () => {
        const indicatorsUI = this.state.products.map((product, key) => {
            if (key === 0) {
                return (
                    <li data-target="#home-carousel" data-slide-to={key} key={key} className="active"></li>
                );
            }
            else {
                return (
                    <li data-target="#home-carousel" ata-slide-to={key} key={key}></li>
                );
            }
        });
        return indicatorsUI;
    }
    render() {
        return (
            <>
                <div id="home-carousel" className="carousel slide" data-ride="carousel">
                    <ul className="carousel-indicators">
                        {this.renderCarouselIndicators()}
                    </ul>
                    <div className="carousel-inner">
                        {this.renderSlider()}
                    </div>
                    <a className="carousel-control-prev" href="#home-carousel" data-slide="prev">
                        <span className="carousel-control-prev-icon"></span>
                    </a>
                    <a className="carousel-control-next" href="#home-carousel" data-slide="next">
                        <span className="carousel-control-next-icon"></span>
                    </a>
                </div>
            </>);
    }
}
export default Slider;