import axios from 'axios';
import {
    // HEADERS,
    BASEURL
} from '../../constant/constant';

export const getRecentProducts = () => {
    let url = `${BASEURL}product/recent-product`;
    return axios.get(url);
}